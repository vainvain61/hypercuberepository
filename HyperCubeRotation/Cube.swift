//
//  Cube.swift
//  HyperCubeRotation
//
//  Created by Sylvain on 2016-08-04.
//  Copyright © 2016 Sylvain Lambert. All rights reserved.
//

import UIKit

    func round2(value: CGFloat, decimalPlaces: Int) -> CGFloat {
        let decimalValue = pow(10.0, CGFloat(decimalPlaces))
        let valeur = round(value * decimalValue) / decimalValue
        
        return CGFloat(valeur)
    }

class Segment3D {
    var start: Point3D
    var end: Point3D
    static let zero = Segment3D(start: Point3D.zero, end: Point3D.zero)
    
    
    init(start: Point3D = Point3D.zero, end: Point3D = Point3D.zero) {
        self.start = start
        self.end = end
    }
}

class Cube {
    var points = [Point3D](count: 8, repeatedValue: Point3D.zero)
    var origine = Point3D()
    let taille: CGFloat
    var segments = [Segment3D]()
    var centre: Point3D  {
        get {
            let centreX = origine.x + taille/2
            let centreY = origine.y + taille/2
            let centreZ = origine.z + taille/2
            return Point3D(x: centreX, y: centreY, z: centreZ)
        }
        set(newCenter) {
            origine.x = newCenter.x - taille/2
            origine.y = newCenter.y - taille/2
            origine.z = newCenter.z - taille/2
            self.constructionVertex()
        }
    }
    
    
    init(origine: Point3D = Point3D.zero, taille: CGFloat = 1) {
        self.origine = origine
        self.taille = taille
        self.constructionVertex()
        self.constructionSegments()
    }

    
    
    func description()  {
        for (index, point) in points.enumerate() {
            let decimal = 2
            print("index:", index, "- coordinate:", round2(point.x, decimalPlaces: decimal), round2(point.y, decimalPlaces: decimal), round2(point.z, decimalPlaces: decimal))
        }
    }
    
    /*
     PRIVATE
     */
    
    private func constructionVertex() {
        for i in 0..<self.points.count {
            var binaire = String(i, radix: 2)
            for _ in 0..<3-binaire.characters.count {
                binaire.insert("0", atIndex: binaire.startIndex)
            }
            let x: Character = binaire[binaire.startIndex.advancedBy(0)]
            let y: Character = binaire[binaire.startIndex.advancedBy(1)]
            let z: Character = binaire[binaire.startIndex.advancedBy(2)]
            
            let point = Point3D(x: transforme(x, facteur: self.taille) + origine.x,
                                y: transforme(y, facteur: self.taille) + origine.y,
                                z: transforme(z, facteur: self.taille) + origine.z)
            points[i] = point
        }
    }
    
    private func transforme(binChar: Character, facteur: CGFloat) -> CGFloat {
        let binaire = Int(String(binChar))!
        let transformed = CGFloat(binaire) * facteur
        
        return transformed
    }
    
    private func constructionSegments() {
        for i in 0..<points.count {
            for j in 0..<i {
                let xor = i ^ j
                if xor == 1 || xor == 2 || xor == 4 {
                    self.segments.append(Segment3D(start: points[i], end: points[j]))
                }
            }
        }
    }
    
    

}
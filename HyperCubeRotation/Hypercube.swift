//
//  Hypercube.swift
//  HyperCubeRotation
//
//  Created by Sylvain on 2016-08-03.
//  Copyright © 2016 Sylvain Lambert. All rights reserved.
//

import UIKit

class Segment4D {
    var start: Point4D
    var end: Point4D
    static let zero = Segment4D(start: Point4D.zero, end: Point4D.zero)
    
    
    init(start: Point4D = Point4D.zero, end: Point4D = Point4D.zero) {
        self.start = start
        self.end = end
    }
}

class Hypercube {
    var points = [Point4D](count: 16, repeatedValue: Point4D.zero)
    var segments = [Segment4D]()
    var origine = Point4D()
    let taille: CGFloat
    var centre: Point4D  {
        get {
            let centreX = origine.x + taille/2
            let centreY = origine.y + taille/2
            let centreZ = origine.z + taille/2
            let centreU = origine.u + taille/2
            return Point4D(x: centreX, y: centreY, z: centreZ, u: centreU)
        }
        set(newCenter) {
            origine.x = newCenter.x - taille/2
            origine.y = newCenter.y - taille/2
            origine.z = newCenter.z - taille/2
            origine.u = newCenter.u - taille/2
            self.constructionVertex()
        }
    }
    
    init(origine: Point4D = Point4D.zero, taille: CGFloat = 1) {
        self.origine = origine
        self.taille = taille
        self.constructionVertex()
        self.constructionSegments()
    }
    
    func description()  {
        for (index, point) in points.enumerate() {
            let decimal = 2
            print("index:", index, "- coordinate:",
                  round2(point.x, decimalPlaces: decimal),
                  round2(point.y, decimalPlaces: decimal),
                  round2(point.z, decimalPlaces: decimal),
                  round2(point.u, decimalPlaces: decimal))
        }
    }
    
    /*
     PRIVATE
     */
    
    private func constructionVertex() {
        for i in 0..<self.points.count {
            var binaire = String(i, radix: 2)
            for _ in 0..<4-binaire.characters.count {
                binaire.insert("0", atIndex: binaire.startIndex)
            }
            let x: Character = binaire[binaire.startIndex.advancedBy(0)]
            let y: Character = binaire[binaire.startIndex.advancedBy(1)]
            let z: Character = binaire[binaire.startIndex.advancedBy(2)]
            let u: Character = binaire[binaire.startIndex.advancedBy(3)]
            
            let point = Point4D(x: transforme(x, facteur: taille) + origine.x,
                                y: transforme(y, facteur: taille) + origine.y,
                                z: transforme(z, facteur: taille) + origine.z,
                                u: transforme(u, facteur: taille) + origine.u)
            points[i] = point

        }
    }
    
    
    private func transforme(binChar: Character, facteur: CGFloat) -> CGFloat {
        let binaire = Int(String(binChar))!
        let transformed = CGFloat(binaire) * facteur
        
        return transformed
    }
    
    private func constructionSegments() {
        for i in 0..<points.count {
            for j in 0..<i {
                let xor = i ^ j
                if xor == 1 || xor == 2 || xor == 4 || xor == 8 {
                    self.segments.append(Segment4D(start: points[i], end: points[j]))
                }
            }
        }
    }
    

}

//
//  Point3D.swift
//  HyperCubeRotation
//
//  Created by Sylvain on 2016-08-03.
//  Copyright © 2016 Sylvain Lambert. All rights reserved.
//

import UIKit

class Point3D {
    var x: CGFloat
    var y: CGFloat
    var z: CGFloat
    
    static let zero = Point3D(x: 0, y: 0, z: 0)
    
    init(x: CGFloat = 0, y: CGFloat = 0, z: CGFloat = 0) {
        self.x = x
        self.y = y
        self.z = z
    }
    
    func projectionParallel2D(centerScreen: CGPoint, facteur: CGFloat = 1, translation: CGPoint = CGPoint.zero) -> CGPoint {
        let pointTransformed = CGPoint(x: centerScreen.x + self.x - facteur*self.z + translation.x,
                                       y: centerScreen.y - (self.y - facteur*self.z + translation.y))
        
        return pointTransformed
    }
    
    func rotationXY(angle: CGFloat) {
        let pt = Point3D(x: self.x, y: self.y, z: self.z)
        self.x = pt.x * cos(angle) - pt.y * sin(angle)
        self.y = pt.x * sin(angle) + pt.y * cos(angle)
    }
    
    func rotationXZ(angle: CGFloat) {
        let pt = Point3D(x: self.x, y: self.y, z: self.z)
        self.x = pt.x * cos(angle) - pt.z * sin(angle)
        self.z = pt.x * sin(angle) + pt.z * cos(angle)
    }
    
    func rotationYZ(angle: CGFloat) {
        let pt = Point3D(x: self.x, y: self.y, z: self.z)
        self.y = pt.y * cos(angle) - pt.z * sin(angle)
        self.z = pt.y * sin(angle) + pt.z * cos(angle)
    }
    
    func scale(facteur: CGFloat) {
        self.x *= facteur
        self.y *= facteur
        self.z *= facteur
    }

}
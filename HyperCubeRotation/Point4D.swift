//
//  Point4D.swift
//  HyperCubeRotation
//
//  Created by Sylvain on 2016-08-03.
//  Copyright © 2016 Sylvain Lambert. All rights reserved.
//

import UIKit

class Point4D {
    var x: CGFloat
    var y: CGFloat
    var z: CGFloat
    var u: CGFloat
    
    static let zero = Point4D(x: 0, y: 0, z: 0, u: 0)
    
    init(x: CGFloat = 0, y: CGFloat = 0, z: CGFloat = 0, u: CGFloat = 0) {
        self.x = x
        self.y = y
        self.z = z
        self.u = u
    }
    
    func projectionParallel3D(facteur: CGFloat = 1) -> Point3D {
        let pointTransformed = Point3D(x: self.x - facteur*self.u,
                                       y: self.y - facteur*self.u,
                                       z: self.z - facteur*self.u)
        
        return pointTransformed
    }
    
    func rotationXU(angle: CGFloat) {
        let pt = Point4D(x: self.x, y: self.y, z: self.z, u: self.u)
        self.x = pt.x * cos(angle) - pt.u * sin(angle)
        self.u = pt.x * sin(angle) + pt.u * cos(angle)
    }
    
    func rotationYU(angle: CGFloat) {
        let pt = Point4D(x: self.x, y: self.y, z: self.z, u: self.u)
        self.y = pt.y * cos(angle) - pt.u * sin(angle)
        self.u = pt.y * sin(angle) + pt.u * cos(angle)
    }
    
    func rotationZU(angle: CGFloat) {
        let pt = Point4D(x: self.x, y: self.y, z: self.z, u: self.u)
        self.z = pt.z * cos(angle) - pt.u * sin(angle)
        self.u = pt.z * sin(angle) + pt.u * cos(angle)
    }
    
    func rotationXY(angle: CGFloat) {
        let pt = Point4D(x: self.x, y: self.y, z: self.z, u: self.u)
        self.x = pt.x * cos(angle) - pt.y * sin(angle)
        self.y = pt.x * sin(angle) + pt.y * cos(angle)
    }
    
    func rotationXZ(angle: CGFloat) {
        let pt = Point4D(x: self.x, y: self.y, z: self.z, u: self.u)
        self.x = pt.x * cos(angle) - pt.z * sin(angle)
        self.z = pt.x * sin(angle) + pt.z * cos(angle)
    }
    
    func rotationYZ(angle: CGFloat) {
        let pt = Point4D(x: self.x, y: self.y, z: self.z, u: self.u)
        self.y = pt.y * cos(angle) - pt.z * sin(angle)
        self.z = pt.y * sin(angle) + pt.z * cos(angle)
    }
    
    func scale(facteur: CGFloat) {
        self.x *= facteur
        self.y *= facteur
        self.z *= facteur
        self.u *= facteur
    }
}

//
//  ViewController.swift
//  HyperCubeRotation
//
//  Created by Sylvain on 2016-08-03.
//  Copyright © 2016 Sylvain Lambert. All rights reserved.
//

import UIKit

let π: CGFloat = 3.1415926536

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let cube = Cube()
//        cube.description()
//        
//        cube.centre = Point3D(x: 1, y: 1, z: 1)
//        cube.description()
//        
//        cube.centre = Point3D(x: 0.5, y: 0.5, z: 0.5)
//        cube.description()
//
//        for point in cube.points {
//            point.rotationXY(π/4)
//        }
//        cube.description()
        
        
        let hypercube = Hypercube()
        hypercube.description()
        
        hypercube.centre = Point4D(x: 1, y: 1, z: 1, u: 1)
        hypercube.description()
        
        hypercube.centre = Point4D(x: 0.5, y: 0.5, z: 0.5, u: 0.5)
        hypercube.description()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

